<?php
// TCPDF Parser
require_once 'Libraries/tcpdf/tcpdf_parser.php';

// PDF Parser
require_once 'Libraries/Smalot/PdfParser/Parser.php';
require_once 'Libraries/Smalot/PdfParser/Document.php';
require_once 'Libraries/Smalot/PdfParser/Object.php';
require_once 'Libraries/Smalot/PdfParser/Header.php';
require_once 'Libraries/Smalot/PdfParser/Element.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementName.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementStruct.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementXRef.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementNumeric.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementBoolean.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementNull.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementString.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementDate.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementHexa.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementArray.php';
#require_once 'Libraries/Smalot/PdfParser/Filters.php';
require_once 'Libraries/Smalot/PdfParser/Element/ElementMissing.php';
require_once 'Libraries/Smalot/PdfParser/Page.php';
require_once 'Libraries/Smalot/PdfParser/XObject/Form.php';
require_once 'Libraries/Smalot/PdfParser/XObject/Image.php';
require_once 'Libraries/Smalot/PdfParser/Font.php';
require_once 'Libraries/Smalot/PdfParser/Font/FontCIDFontType2.php';
require_once 'Libraries/Smalot/PdfParser/Font/FontCIDFontType0.php';
require_once 'Libraries/Smalot/PdfParser/Font/FontType0.php';
require_once 'Libraries/Smalot/PdfParser/Page.php';
require_once 'Libraries/Smalot/PdfParser/Pages.php';
require_once 'Libraries/Smalot/PdfParser/Font/FontTrueType.php';
require_once 'Libraries/Smalot/PdfParser/Font/FontType1.php';

// HTTP Client
require_once 'Libraries/HttpClient.php';

// DeathByCaptcha.com API
require_once 'Libraries/DeathByCaptcha.php';

// Registro Civil API
require_once 'Libraries/RegistroCivil.php';