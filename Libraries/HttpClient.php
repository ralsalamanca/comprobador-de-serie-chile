<?php
namespace Libraries;

class HttpClient{
	var $cookies = array();
	var $last_request;
	function post($url, $data=array()){
		return $this->__request('POST', $url, $data);
	}
	function get($url, $data=array()){
		return $this->__request('GET', $url, $data);
	}
	private function __request($method, $url, $data=array()){
		$url	= parse_url($url);
		$port	= @$url['port']?$url['port']:($url['scheme']=='https'?443:80);
		$socket = fsockopen(($url['scheme']=='https'?'ssl://':'') . $url['host'], $port, $errno, $errstr, 5);
		if (!$socket) {
			return false;
		} else {
			$request = '';
			$request .= "{$method} /{$url['path']}".(@$url['query']?'?'.$url['query']:'')." HTTP/1.1\r\n";
			$request .= "Host: {$url['host']}\r\n";
			$cookies = '';
			foreach($this->cookies as $cookie){
				$cookies .= "{$cookie->name}={$cookie->value},";
			}
			if(strlen($cookies)>0){
				$request .= "Cookie: ".substr($cookies, 0, -1)."\r\n";
			}
			if($method=='POST'){
				$data = http_build_query($data);
				$request .= "Content-Type: application/x-www-form-urlencoded\r\n";
				$request .= "Content-Length: " . strlen($data) . "\r\n";
			}
			$request .= "Connection: close\r\n\r\n";
			if($method=='POST'){
				$request .= $data;
			}
			
			fwrite($socket, $request);
			$response = '';
			while (!feof($socket)) {
				$response .= fgets($socket, 128);
			}
			$response = $this->__parse_response($response);
			foreach($response->cookies as $cookie_name => $cookie){
				$this->cookies[$cookie_name] = $cookie;
			}
			return $response->content;
			fclose($socket);
		}
	}
	private function __parse_response($response){
		list($headers, $content) = explode("\r\n\r\n", $response, 2);
		$_headers = $this->__parse_headers($headers);
		$this->last_request = $response = (object)array(
			'status'  => current(explode('\r\n', $headers)),
			'headers' => $_headers,
			'cookies' => $this->__extract_cookies($_headers),
			'content' => trim($content)
		);

		return $response;
	}
	private function __parse_headers($headers){
		$_headers = array(); 
		$headers  = explode("\r\n", $headers);
		array_shift($headers);
		foreach($headers as $header){
			list($key, $value) = explode(':', $header);
			
			$key   = strtolower(trim($key));
			$value = trim($value);
			
			if(array_key_exists($key, $_headers)){
				if(!is_array($_headers[$key])){
					$_headers[$key] = array($value);
				}
				$_headers[$key][] = $value;
			}
			else{
				$_headers[$key] = $value;
			}
		}
		return $_headers;
	}
	private function __extract_cookies($headers){
		if(@$headers['set-cookie']){
			if(!is_array($headers['set-cookie']))
				$headers['set-cookie'] = array($headers['set-cookie']);
			foreach($headers['set-cookie'] as $setcookie){
				$setcookies = explode(',', $setcookie);
				foreach($setcookies as $cookie){
					$attribs = explode(';', $cookie);
					list($cookie_name, $cookie_value) = explode('=', $attribs[0]);
					array_shift($attribs);
					foreach($attribs as $attrib){
						if(strstr($attrib, '=')){
							list($key, $value) = explode('=', $attrib);
							$_attribs[strtolower(trim($key))] = trim($value);
						}
					}
					$cookies[$cookie_name] = (object)array_merge(array(
						'name'	=> $cookie_name,
						'value' => $cookie_value
					), $_attribs);
				}
			}
			return $cookies;
		}
		else{
			return array();
		}
	}
}
?>