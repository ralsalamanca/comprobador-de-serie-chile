<?php
namespace Libraries;

use \Smalot\PdfParser\Parser;

class RegistroCivil{
	var $http;
	var $pdfparser;
	var $certificates = array(
		0 => '1_2',
		1 => '1_3',
		2 => '1_4'
	);
	function __construct($captchaClass){
		$this->http			= new HttpClient;
		$this->pdfparser	= new \Smalot\PdfParser\Parser();
		$this->client		= $captchaClass;
	}
	function getDataFromCertificate($rut, $requester, $type=0){
		if($certificate = $this->getCertificate($rut, $requester, $type)){
			$certificate	= utf8_decode($this->pdfparser->parseContent($certificate)->getText());
			$data			= (object)array();
			$data->father	= (object)array();
			$data->mother	= (object)array();
			foreach(explode("\n", $certificate) as $line){
				@list($name, $value) = explode(':', $line, 2);
				if($type==0){
					$value = trim($value);
					$name  = strtr(str_replace(array("\r", "\n", "\t", " "), '', trim($name)),'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
					switch($name){
						case 'Nombreinscrito':
							$data->name				= $value;
							break;
						case 'R.U.N.':
							$data->run				= $value;
							break;
						case 'Fechanacimiento':
							$data->birth_date		= $value;
							break;
						case 'Sexo':
							$data->sex				= $value;
							break;
						case 'Nombredelpadre':
							$data->father->name		= $value;
							break;
						case 'R.U.N.delpadre':
							$data->father->run		= $value;
							break;
						case 'Nombredelamadre':
							$data->mother->name		= $value;	
							break;
						case 'R.U.N.delamadre':
							$data->mother->run		= $value;
							break;
					}
					if(stristr($name, 'Circunscripci')){
						$data->circunscription	= $value;
					}
					elseif(stristr($name, 'Nro.inscripci')){
						$inscription			= explode(' ', $value);
						$data->id				= (int)str_replace('.', '', $inscription[0]);
						$data->year				= (int)end($inscription);
					}
				}
			}
			return $data;
		}
		else{
			return false;
		}
	}
	function getCertificate($rut, $requester, $type=0){
		$rut	= str_replace(array('.', '-'), '', $rut);
		$dv		= substr($rut, -1);
		$rut	= substr($rut, 0, -1);
		
		$adquirirCertificado = $this->http->get('https://www.registrocivil.cl/OficinaInternet/adquirirCertificado.do');
		#echo $adquirirCertificado;
		#exit;
		preg_match("/ii=(.*)\"/U", $adquirirCertificado, $result);
		if($captcha = $this->client->decode($this->http->get("https://www.registrocivil.cl/OficinaInternet/generaCaptchaAction.do?ii={$result[1]}"))){
			if(stristr($this->http->post('https://www.registrocivil.cl/OficinaInternet/obtenerPaso1.do', array(
				'muestra'	=> '3', 
				'accion'	=> 'obtener',
				'runFree'	=> $rut,
				'dvFree'	=> $dv,
				'certFree'	=> $this->certificates[$type],
				'llave'		=> $captcha['text']
			)), 'ingresado es incorrecto, intentelo nuevamente por favor')){
				$this->client->report($captcha['captcha']);
				return false;
			}
			$this->http->post('https://www.registrocivil.cl/OficinaInternet/obtenerPaso2.do', array(
				'accion'		=> '',
				'nombre'		=> $requester->name,
				'paterno'		=> $requester->lastname1,
				'materno'		=> $requester->lastname2,
				'region'		=> $requester->region,
				'fono_cod'		=> '',
				'fono'			=> '',
				'celular_cod'	=> '',
				'celular'		=> '',
				'email'			=> $requester->email,
				'email2'		=> $requester->email
			));
			preg_match('|"/OficinaInternet/servlet/docs(.*)"|U', $this->http->get('https://www.registrocivil.cl/OficinaInternet/obtenerPaso3.do'), $result);
			
			if($response = $this->http->get('https://www.registrocivil.cl/OficinaInternet/servlet/docs' . $result[1]))
				return $response;
			else
				return false;
		}
	}
}
?>